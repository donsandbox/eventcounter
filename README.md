Instrumental

This project is designed to implement a library for the purpose of tracking instances of an event over a period of time.
To use this library:
Create the event-tracking object using Counter::GenerateCounter().  This provides a pointer to a counter object for use in subsequent functions.  The input parameters are as follows:
    maxTimeout  - amount of time to pass before triggering new events cause deletion of old ones (seconds).
    granularity     - accuracy/grouping of events (seconds).
Trigger an event using Counter::IncrementCounter().  This event will compare itself to previious events and if they are outside the granularity, a new chunk of memory will be consumed to track this event.  Then if the oldest chunk of memory is older than the max timeout, it is destroyed and all events it accounted for are lost.  NOTE:  because the number of events per chunk is tracked by an int, it is possible to overload the number of events in a chunk of memory.  However, 2^31 is plenty to account for the "millions of hits per second" specified (assuming granularity of 1 second).
    counter         - pointer to the object tracking this information
Get all events in a period of time using Counter::GetCount().  The function will iterate over the chunks of memory until it finds a chunk of memory older than the given timeout, and returns everything beforehand.  NOTE:  if the user/default timeout is longer than what the counter is meant to track, the value becomes negative to signify this alert.
    counter         - pointer to the object tracking this information
    timeout         - time window for finding recent events
Destroy the object by calling Counter::DestroyCounter().  This will call the destructor on the linked list and then on the counter itself.  Calling this will render the pointer corrupt.
    counter         - pointer to object to be destroyed
    
Extra Notes and Edge Cases:
This library assumes all non-null pointers are in fact references to legal counter objects.  If a pointer references either an invalid counter or some other type of object, the library will behave in unexpected means.
This library assumes all primitive data is of the correct datatype when passed in.  Ints are ints, doubles are doubles, etc.
This library handles four main edge cases when geting quantity of events within a certain time window:
    No events stored - will attempt to iterate over NULL pointers, return 0
    Time window of 0 seconds indicated - will default to counter's max timeout
    Negative time window - will attempt to verify items against a negative timeframe, return 0
    Timeframe exceeds max time - will return a negative number if nonzero, to indicate this is outside the counter's normal tracking, and the value cannot be trusted for precision work
    

