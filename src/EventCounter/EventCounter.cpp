﻿// EventCounter.cpp : Defines the exported functions for the DLL application.
//

#include "EventCounter.h"

using namespace EC;

EventNode::EventNode(time_t time)
{
	timestamp = time;
	numEvents = 1;
	newer = NULL;
	older = NULL;
}

EventNode::~EventNode()
{
	if (older)
		delete older;
}

Counter::Counter(double time, double gran)
{
	maxTimeout = time;
	granularity = gran;
	newest = NULL;
	oldest = NULL;
}

Counter::~Counter()
{
	if (newest)
		delete newest;
}

// Function to return a pointer to a counter object for later use
// Takes in a maximum timeout value (defaulted to 5 minutes)
Counter * Counter::GenerateCounter(double timeout, double granularity)
{
	return new Counter(timeout, granularity);
}

// Function to note that an event has triggered a single time
// Takes in a counter object to operate on
// Will either append to current node if within granularity window, or create new node
// Intensity in creating new nodes is minimalized because it can only happen max once per unit of granularity
// Afterwards, last list item is checked for trimming if a new one is appended (not thread safe, but not a requirement to be)
void Counter::IncrementCounter(Counter *counter)
{
	if (!counter) return;

	EventNode *newEventNode, *currEventNode = counter->newest, *oldestEventNode;
	time_t timestamp = time(NULL);

	// Decide if the event is logged in the current node or a new one
	if (currEventNode && difftime(timestamp, currEventNode->timestamp) < counter->granularity)
		currEventNode->numEvents++;
	else
	{
		newEventNode = new EventNode(timestamp);
		newEventNode->older = currEventNode;
		counter->newest = newEventNode;
		if (currEventNode)
			currEventNode->newer = newEventNode;
		else
			counter->oldest = newEventNode;
		
		// Cleanup oldest node if needed (only performed if a new one has been generated)
		oldestEventNode = counter->oldest;
		if (oldestEventNode && difftime(timestamp, oldestEventNode->timestamp) > counter->maxTimeout)
		{
			counter->oldest = oldestEventNode->newer;
			counter->oldest->older = NULL;
			delete oldestEventNode;
		}
	}
	return;
}

// Function to traverse nodes and tally all events within a time window
// Function also clears out data older than counter's maxTimeout value
// If timeout is greater than the counter's maxTimeout threshold, make the value negative to signify an issue
// Takes in a counter object to operate on
// Takes in a timeout function to judge event nodes by
int Counter::GetCount(Counter *counter, double timeout)
{
	if (!counter) return 0;
	if (!timeout) timeout = counter->maxTimeout;

	EventNode *currEventNode = counter->newest;
	int eventsInInterval = 0;
	time_t currentTime = time(NULL);
	double diffTime;

	// Find the total number of events in said time
	while (currEventNode)
	{
		diffTime = difftime(currentTime, currEventNode->timestamp);
		if (diffTime < timeout)
		{
			eventsInInterval += currEventNode->numEvents;
			currEventNode = currEventNode->older;
		}
		else
			break;
	}

	// Cleanup all nodes older than the maxTimeout
	// Commented out from functional code to increase speed of get() query
	// Remains in the code as another way to demonstrate cleanup
	/*
	while (currEventNode)
	{
		diffTime = difftime(currentTime, currEventNode->timestamp);
		if (diffTime > counter->maxTimeout)
		{
			counter->oldest = currEventNode->newer;
			counter->oldest->older = NULL;
			delete currEventNode;
			break;
		}
		currEventNode = currEventNode->older;
	}
	*/

	// Modify value to demonstrate incomplete data
	if (timeout > counter->maxTimeout)
		eventsInInterval *= -1;

	return eventsInInterval;
}

// Function to destroy a counter object and corresponding linked list of data
// Takes in a counter object to be destroyed
void Counter::DestroyCounter(Counter *counter) 
{
	if (!counter) return;

	delete counter;
	return;
}
