﻿#pragma once 

#include <ctime>

#define DEFAULT_TIMEOUT 5*60	// Default max timeout, after which items will be deleted - 5 minutes
#define DEFAULT_GRANULARITY 1	// Default granularity for grouping instances of one event (per node) - 1 second

namespace EC
{
	// Class to handle each instance of a type of event to occur across some duration (granularity)
	// Meant to be strung in a doubly-linked list
	// timestamp - indicates time at which event happened
	// numEvents - number of instances the event was triggered for this duration (granularity)
	// newer - pointer to next newest instance, for creating new nodes
	// older - pointer to next oldest instance, for destroying old nodes
	class EventNode
	{
	public:
		time_t	timestamp;
		int numEvents;
		EventNode	*newer, *older;

		EventNode(time_t time = NULL);
		~EventNode();
	};

	// Class to handle all instances of a type of event
	// maxTimeout - threshold after which older items are deleted
	// granularity - grouping of events, used to improve storage, tracking and deletion
	// newest - pointer to newest item (head of list)
	class Counter
	{
		double		maxTimeout, granularity;
		EventNode	*newest, *oldest;

	public:
		Counter(double time = DEFAULT_TIMEOUT, double gran = DEFAULT_GRANULARITY);
		~Counter();
		//Export Functions, designed to be stateless, and either provide pointers or operate on provided pointers
		static void		IncrementCounter(Counter* counter);
		static int		GetCount(Counter* counter, double timeout = 0);
		static Counter*	GenerateCounter(double timeout = DEFAULT_TIMEOUT, double granularity = DEFAULT_GRANULARITY);
		static void		DestroyCounter(Counter * counter);
	};
}